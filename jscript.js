/* 
create a JS function that takes an array of numbers, sorts them from the smallest to the largest one, picks out the 2nd smallest and the 2nd largest number, and then pushes them
both into an empty array, and finally joins them as a string.
*/

// 1st solution

function quickMaths(arr) {
    //sort the numbers in an array (lowest to highest) - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
    var sorting = arr.sort(function(a, b){
        return a - b
    }); 
    //define the finalResult as an empty array - sorted numbers will be pushed into it
    var finalResult = [];
    //choose the 2nd smallest number ([1]) in the sorted result (sorting) - https://www.w3schools.com/js/js_array_sort.asp
    var secondSmallest = sorting[1]; 
    finalResult.push(secondSmallest); 
    //choose the 2nd largest number ([5]) in the sorted result (sorting) - or use sorting[sorting.length-2]
    var secondLargest = sorting[5]; 
    finalResult.push(secondLargest); 
    //join the 2 elements of the "finalResult" array into a string with "and"
    return finalResult.join(" and "); 
}
arr = [13,105,75,1,344,6,5];
console.log(arr);
console.log("Second smallest and second largest numbers are " + quickMaths(arr));

// 2nd (better) solution with a for loop

function quickMaffs(arr){
    //sort the numbers in an array (lowest to highest)
    arr.sort(function(a, b){
        return a - b;
    });
    // "reza" will hold only unique numbers - check out the for loop
    var reza = [arr[0]];
    //define the variable as an empty array - 2 sorted numbers are pushed inside
    var finalResult = [];
    
    //loop through all of the elements of the sorted array and push them into reza
    for(var i=1; i < arr.length; i++){
        //check if the current number is the same as the previous one - if it is, then ignore it i.e. what if the array was [1,105,5,1,344,344,5]? 1st solution messes it up.
        if(arr[i-1] !== arr[i]){
        //if the current number is NOT the same as the previous one, then push it into "reza"
        reza.push(arr[i]);
    }
    }
    //push the 2nd smallest ([1]) and the 2nd largest ([5]) from "reza" array into the "finalResult" array
    finalResult.push(reza[1]);
    finalResult.push(reza[reza.length-2]);
    //join the 2 elements of the "finalResult" array into a string with "and"
    return finalResult.join(" and ");
}
    
console.log("Second smallest and second largest numbers are " + quickMaffs([13,105,75,1,344,6,5]));

